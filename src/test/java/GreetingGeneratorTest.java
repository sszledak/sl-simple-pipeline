import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GreetingGeneratorTest {

    @Test
    void shouldGenerateGreetingCorrectly() {
        GreetingGenerator greetingGenerator = new GreetingGenerator();
        assertEquals("Hello World", greetingGenerator.greeting("World"));
    }
}